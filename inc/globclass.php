<?php
/*
author	: Andika Hermansyah
create	: 31 March 2011
include	: mysql and template

modif 	: 12 November 2015
versi	: 1.3
- Add Multi Database
- Add fecth object

modif 	: 28 Agustus 2014
versi	: 1.2.3
- Fix tag php 5.4

modif	: 30 November 2011
versi	: 1.2.2
*/

class dbC{ 
protected $myhost, $myuser, $mypassword, $mydb, $dbconn;
var $sqlstore, $sqlfield;
	public function __construct() {
		require($_SERVER['DOCUMENT_ROOT'].'/confix.php');
		$this->myhost = $Xhost;
		$this->myuser = 'jquedcom_aku';
		$this->mypassword = 'poiqwe22-=';
		$this->mydb = 'jquedcom_money';
		$this->connect();
	}
	
	function connect(){ 
		$this->dbconn = @mysql_pconnect($this->myhost,$this->myuser,$this->mypassword) or die("Connection to the server failed");
		@mysql_select_db($this->mydb) or die("No such database exist"); 
		return $this->dbconn; 
	} 
	
	function query($sql){ 
		$this->sqlstore = @mysql_query($sql) or die("There is error in the sql query ".$sql.' -> '.mysql_error()); 
		return $this->sqlstore; 
	} 
	
	function qaray($sql){ 
		return @mysql_fetch_array($sql);
	}

	function qobject($sql){ 
		return @mysql_fetch_object($sql);
	}
	
	function qray($sql){ 
		$rowfield = @mysql_query($sql) or die("There is error in the sql query " .mysql_error());
		$this->sqlfield = @mysql_fetch_array($rowfield);
		return $this->sqlfield;
	} 
	
	function qnum($sql){ 
		$rowfield = @mysql_query($sql) or die("There is error in the sql query " .mysql_error());
		$this->sqlfield = mysql_num_rows($rowfield);
		return $this->sqlfield; 
	} 
}

class getTpl{
	var $vars = array(), $varsX = array(), $file;
	
	function set_var($varname, $varval){
        $this->vars[] = '<!--'.$varname.'!-->';
        $this->varsX[] = $varval;
        return true;
    }
	
	function set_file($set_tpl) {
		return $this->file = $set_tpl;
	}
	
	function vTpl() {
		extract($this->vars);
		include($this->file);
	}
	
	function vTplX() {
		//require($_SERVER['DOCUMENT_ROOT'].'/confweb.php');
		$minHtml=1;
		$b=file_get_contents($this->file);
		$a=str_replace($this->vars,$this->varsX,$b);
		if($minHtml){
			$c=array("\n", "\t");
			$a=str_replace($c,'',$a);
		}		
		echo trim($a);
	}
}

class getTimeDebug{
	var $tmStart, $tmEnd;
	public function __construct() {
		$this->tmStart=microtime(true);
		/*require($_SERVER['DOCUMENT_ROOT'].'/cache/counter.php');
		$this->counterVal = $count;
		$this->setCount();	*/	
	}
	
	function getEnd(){
		usleep(100);
		$this->tmEnd=microtime(true);
		$result=$this->tmEnd - $this->tmStart;		
		return $result;
	}
}

class getCount{
	var $counterVal, $nextCount;
	public function __construct() {
		require($_SERVER['DOCUMENT_ROOT'].'/cache/counter.php');
		$this->counterVal = $count;
		$this->setCount();		
	}
	function setCount(){ 		
		if(!isset($_SESSION['hasVisited'])){
			$this->nextCount=$this->counterVal + 1;
			$_SESSION['hasVisited']=$this->nextCount;
			setcookie('hasVisited',1,time() + (86400)); // 86400 = 1 day
			//echo 'Hello '.($_COOKIE['first_name']!='' ? $_COOKIE['first_name'] : 'Guest'); // Hello David!
			//http://davidwalsh.name/php-cookies
			
			$buffer = "<?php
\$count = '$this->nextCount';
?>";
			$f = fopen($_SERVER['DOCUMENT_ROOT'].'/cache/counter.php', "w");
			fwrite($f, $buffer);
			fclose($f);
		}else{
			$this->nextCount=$_SESSION['hasVisited'];
		}
	} 
	function getCountVisit(){
		return $this->nextCount; 
	}
}

function cekThis($val){
	if (get_magic_quotes_gpc()) {
		$valid = $val;    	
	}else{
    		$valid = addslashes($val);
	}
	return $valid;
}

function buildSidebar(){
	$s = new dbC;
	$q=$s->query("select * from sidebar where showit=1");
	$xside='<div class="sideright"><div class="otherside">';
	while($r=$s->qaray($q)){
		$xtitle= ($r['title']!='') ? '<div class="titleSide">'.$r['title'].'</div>'.stripslashes($r['text']) : stripslashes($r['text']);
		$xside.='<div class="otherside">'.$xtitle.'</div>';
	}
	$xside.='</div>';
	$xside=str_replace("'","\'",$xside);
	$buffer="<?php
\$sidebar = '$xside';
?>";
	$f = fopen($_SERVER['DOCUMENT_ROOT'].'/cache/sidebar.php', "w");
	fwrite($f, $buffer);
	fclose($f); 
}
?>